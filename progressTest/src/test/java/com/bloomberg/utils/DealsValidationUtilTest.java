package com.bloomberg.utils;

import com.bloomberg.dto.BaseDealsDto;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rpun on 1/21/2018.
 */
public class DealsValidationUtilTest {

    @Test
    public void givenAmount_whenGreaterThanZero_thenValidAmount() {
        DealsValidationUtil.isValidDealAmount(Double.parseDouble("10"));
        Assert.assertTrue(true);
    }

    @Test
    public void givenAmount_whenSmallerThanZero_thenInvalidAmount() {
        DealsValidationUtil.isValidDealAmount(Double.parseDouble("-10"));
        Assert.assertFalse(false);
    }

    @Test
    public void givenCurrencyISOCode_whenMatchesWithValidCurrencyISOCodeList_thenValidCurrencyISOCode() {
        DealsValidationUtil.isValidCountryCode("AED");
        Assert.assertTrue(true);
    }

    @Test
    public void givenCurrencyISOCode_whenDoesNotMatchWithValidCurrencyISOCodeList_thenInvalidCurrencyISOCode() {
        DealsValidationUtil.isValidCountryCode("HHH");
        Assert.assertFalse(false);
    }


    @Test
    public void givenDeal_whenAllFieldsAreValid_thenValidDeal() {
        BaseDealsDto deals = new BaseDealsDto();
        deals.setFromCurrencyIsoCode("AUS");
        deals.setToCurrencyIsoCode("AED");
        deals.setAmount(Double.parseDouble("20"));
        DealsValidationUtil.isDealValid(deals);
        Assert.assertTrue(true);
    }


    @Test
    public void givenDeal_whenAnyFieldInvalid_thenInvalidDeal() {
        BaseDealsDto deals = new BaseDealsDto();
        deals.setFromCurrencyIsoCode("HHH");
        deals.setToCurrencyIsoCode("AED");
        deals.setAmount(Double.parseDouble("20"));
        DealsValidationUtil.isDealValid(deals);
        Assert.assertFalse(false);
    }


}
