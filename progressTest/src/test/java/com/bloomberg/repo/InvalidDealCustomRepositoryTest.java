package com.bloomberg.repo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by rpun on 2/3/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class InvalidDealCustomRepositoryTest {

    private InvalidDealCustomRepository invalidDealCustomRepository;
    @Mock
    private InvalidDealRepository invalidDealRepository;

    @Before
    public void setUp() {
        invalidDealCustomRepository = new InvalidDealCustomRepository(invalidDealRepository);
    }

    @Test
    public void givenDealsEmpty_whenSaveDeals_thenVerifyDbCall() {
        invalidDealCustomRepository.saveDeals(new ArrayList<>());
        verify(invalidDealRepository).save(any(List.class));
    }

    @Test
    public void givenInvalidDeals_whenSaveDeals_thenSaveDeals() {
        invalidDealCustomRepository.saveDeals(new ArrayList<>());
        verify(invalidDealRepository).save(any(List.class));
    }
}
