package com.bloomberg.repo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by rpun on 2/3/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class ValidDealCustomRepositoryTest {

    private ValidDealCustomRepository validDealCustomRepository;
    @Mock
    private ValidDealRepository validDealRepository;


    @Before
    public void setUp() {
        validDealCustomRepository = new ValidDealCustomRepository(validDealRepository);
    }

    @Test
    public void givenDealsEmpty_whenSaveDeals_thenVerifyDbCall() {
        validDealCustomRepository.saveDeals(new ArrayList<>());
        verify(validDealRepository).save(any(List.class));
    }


    @Test
    public void givenValidDeals_whenSaveDeals_thenSaveOnDeals() {
        validDealCustomRepository.saveDeals(new ArrayList<>());
        verify(validDealRepository).save(any(List.class));
    }
}