package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import com.bloomberg.domain.ValidDeals;
import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.repo.ValidDealRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rpun on 1/26/2018.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {
        ValidDealService.class
})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ValidDealServiceTest {

    private final String DEAL_FILE_NAME = "deals20180126223549.csv";

    private final String RANDOM_FILE_NAME = "mmmm.csv";

    private static ValidDealService mockValidDealService;


    @Mock
    private ValidDealService validDealService;

    @Mock
    private ValidDealRepository validDealRepository;

    @Before
    public void init() {
        mockValidDealService = mock(ValidDealService.class);
        BaseDealsDto validBaseDealsDtoSample1 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfe9b3893d3", "USA", "AUS", "2018-01-2621:46:17", 588.27);
        Deals deals = new Deals();
        ValidDeals validDeals1 = new ValidDeals(validBaseDealsDtoSample1, deals);
        List<ValidDeals> validDeals = Arrays.asList(validDeals1);
        String fileName = "sample.csv";
        when(mockValidDealService.getValidDealByFileName(fileName)).thenReturn(validDeals);


    }


    @Test
    public void givenFileName_whenValidDealWithFileNameExist_thenValidDealExist() {
        String fileName = "sample.csv";
        Assert.assertTrue(mockValidDealService.getValidDealByFileName(fileName).size() > 0);
    }

}
