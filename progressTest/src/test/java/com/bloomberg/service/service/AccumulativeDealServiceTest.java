package com.bloomberg.service.service;

import com.bloomberg.domain.AccumulateDealsCount;
import com.bloomberg.domain.Deals;
import com.bloomberg.domain.ValidDeals;
import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.repo.AccumulativeDealRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by rpun on 2/5/2018.
 */

@RunWith(MockitoJUnitRunner.class)
public class AccumulativeDealServiceTest {

    @Mock
    AccumulativeDealRepository accumulativeDealRepository;

    @Before
    public void setUp() throws Exception {
        AccumulateDealsCount accumulateDealsCount1 = new AccumulateDealsCount();
        accumulateDealsCount1.setCurrencyIsoCode("AED");
        accumulateDealsCount1.setDealsCount(100);

        AccumulateDealsCount accumulateDealsCount2 = new AccumulateDealsCount();
        accumulateDealsCount2.setCurrencyIsoCode("AUS");
        accumulateDealsCount2.setDealsCount(50);

        List<AccumulateDealsCount> totalCount = Arrays.asList(accumulateDealsCount1, accumulateDealsCount2);


        when(accumulativeDealRepository.getAccumulateDealsCountsByFromCurrencyISOCode()).thenReturn(totalCount);

        when(accumulativeDealRepository.getAccumulateDealsCountByCurencyIsoCode("AED")).thenReturn(accumulateDealsCount1);


    }

    @Test
    public void given_newValidDeals_thenCountAndAccumulateOldAndNewValidDeals() {
        List<AccumulateDealsCount> getOldCounts = accumulativeDealRepository.getAccumulateDealsCountsByFromCurrencyISOCode();
        Map<String, Long> countsOfOldAccumulativeDealsByCurrencyIsoCode = getOldCounts.stream().collect(Collectors.toMap
                (AccumulateDealsCount::getCurrencyIsoCode, p -> (long) p.getDealsCount()));


        BaseDealsDto invalidBaseDealsDto1 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfe9b3893d3", "AED", "AUS", "2018-01-2621:46:17", -588.27);
        BaseDealsDto invalidBaseDealsDto2 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfh9b38936r", "AUS", "GBP", "2018-01-2621:46:17", -900.78);
        Deals deals = new Deals();
        ValidDeals validDeals1 = new ValidDeals(invalidBaseDealsDto1, deals);
        ValidDeals validDeals2 = new ValidDeals(invalidBaseDealsDto2, deals);
        List<ValidDeals> validDeals = Arrays.asList(validDeals1, validDeals2);
        Map<String, Long> countsOfNewAccumulativeDealsByCurrencyIsoCode = validDeals.stream().collect(Collectors.groupingBy(ValidDeals::getFromCurrencyIsoCode, Collectors.counting()));
        Map<String, Long> getTotalValidAccumulativeDeals = Stream.of(countsOfOldAccumulativeDealsByCurrencyIsoCode, countsOfNewAccumulativeDealsByCurrencyIsoCode)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                Long::sum
                        )
                );
        Assert.assertTrue(getTotalValidAccumulativeDeals.get("AED") == 101);

    }

    @Test
    public void givenValidDealsAndCount_whenSaveDeals_thenSaveDeals() {
        accumulativeDealRepository.save(new ArrayList<>());
        verify(accumulativeDealRepository).save(any(List.class));
    }

    @Test
    public void givenCurrencyCode_ifExistsInDb_thenReturnCount() {
        AccumulateDealsCount ac = accumulativeDealRepository.getAccumulateDealsCountByCurencyIsoCode("AED");
        Assert.assertTrue(ac.getDealsCount() > 0);
    }
}
