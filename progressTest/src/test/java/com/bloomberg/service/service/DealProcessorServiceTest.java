package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import com.bloomberg.domain.ValidDeals;
import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.utils.DealsValidationUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by rpun on 1/26/2018.
 */
public class DealProcessorServiceTest {

    @Test
    public void givenBaseDealsDtoList_whenValidDeals_thenShouldCollectToValidDealsList() {
        BaseDealsDto validBaseDealsDtoSample1 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfe9b3893d3", "USA", "AUS", "2018-01-2621:46:17", 588.27);
        BaseDealsDto validBaseDealsDtoSample2 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfh9b38936r", "AUS", "GBP", "2018-01-2621:46:17", 900.78);
        Deals deals = new Deals();
        ValidDeals validDeals1 = new ValidDeals(validBaseDealsDtoSample1, deals);
        ValidDeals validDeals2 = new ValidDeals(validBaseDealsDtoSample2, deals);
        List<ValidDeals> actualValidDeals = Arrays.asList(validDeals1, validDeals2);
        List<ValidDeals> expectedValidDeals = Arrays.asList(validDeals1, validDeals2);

        Assert.assertEquals(DealsValidationUtil.isDealValid(validBaseDealsDtoSample1), true);

        Assert.assertEquals(actualValidDeals, expectedValidDeals);
    }

    @Test
    public void givenBaseDealsDtoList_whenInvalidDeals_thenShouldCollectToInvalidDealsList() {
        BaseDealsDto invalidBaseDealsDto1 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfe9b3893d3", "USA", "AUS", "2018-01-2621:46:17", -588.27);
        BaseDealsDto invalidBaseDealsDto2 = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfh9b38936r", "AUS", "GBP", "2018-01-2621:46:17", -900.78);
        Deals deals = new Deals();
        ValidDeals validDeals1 = new ValidDeals(invalidBaseDealsDto1, deals);
        ValidDeals validDeals2 = new ValidDeals(invalidBaseDealsDto2, deals);
        List<ValidDeals> actualValidDeals = Arrays.asList(validDeals1, validDeals2);
        List<ValidDeals> expectedValidDeals = Arrays.asList(validDeals1, validDeals2);

        Assert.assertEquals(!DealsValidationUtil.isDealValid(invalidBaseDealsDto1), true);

        Assert.assertEquals(actualValidDeals, expectedValidDeals);
    }
}
