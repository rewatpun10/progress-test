package com.bloomberg.service.service;

import com.bloomberg.repo.InvalidDealCustomRepository;
import com.bloomberg.repo.ValidDealCustomRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.IOException;

/**
 * Created by rpun on 1/27/2018.
 */
@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration
@TestPropertySource("/application.properties")

public class FileServiceTest {
    @Mock
    private DealService dealService;

    @Mock
    private DealProcessorService dealProcessorService;

    @Mock
    private AccumulativeDealService accumulativeDealService;

    @Mock
    private InvalidDealCustomRepository invalidDealCustomRepository;

    @Mock
    private ValidDealCustomRepository validDealCustomRepository;

    private FileService fileService;


    @Value("${bloomberg.UploadedFilePath}")
    private String path;


    @Before
    public void init() {
        fileService = new FileService(dealService, dealProcessorService, accumulativeDealService, invalidDealCustomRepository, validDealCustomRepository);


    }

    @Test
    public void givenCSVFile_whenUploading_thenFileShouldBeUploaded() throws IOException {

        final String fileName = "deals20180122015149.csv";

        String filePath = path + "\\deals20180122015149.csv";
        final byte[] content = filePath.getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, "application/octet-stream", content);

        String message = fileService.startUploadFile(mockMultipartFile);
        Assert.assertEquals("File uploaded successfully.", message);

    }

    @Test
    public void givenCsvFile_whenFileExtensionIsNotCsv_thenFileShouldNotBeUploaded() throws IOException {

        final String fileName = "deals20188880122015189.txt";

        String filePath = path + "\\dea777ls2018012201518849.txt";
        final byte[] content = filePath.getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, "application/octet-stream", content);

        String message = fileService.startUploadFile(mockMultipartFile);
        Assert.assertEquals("Please upload file with .csv extension.", message);

    }

}
