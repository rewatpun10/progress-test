package com.bloomberg.service.service;

import com.bloomberg.dto.UploadedDealsFileDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rpun on 1/27/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class UploadDealFileServiceTest {


    private static UploadedDealsFileService mockUploadedDealsFileService;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockUploadedDealsFileService = mock(UploadedDealsFileService.class);

        UploadedDealsFileDto uploadedDealsFileDto1 = new UploadedDealsFileDto();
        uploadedDealsFileDto1.setDealEndDate(new Date());
        uploadedDealsFileDto1.setDealStartDate(new Date());
        uploadedDealsFileDto1.setFileName("HHHHH.csv");
        uploadedDealsFileDto1.setValidDealsCount(100);
        uploadedDealsFileDto1.setInvalidDealsCount(100);

        UploadedDealsFileDto uploadedDealsFileDto2 = new UploadedDealsFileDto();
        uploadedDealsFileDto1.setDealEndDate(new Date());
        uploadedDealsFileDto1.setDealStartDate(new Date());
        uploadedDealsFileDto1.setFileName("bbbbb.csv");
        uploadedDealsFileDto1.setValidDealsCount(20);
        uploadedDealsFileDto1.setInvalidDealsCount(20);

        List<UploadedDealsFileDto> uploadedFiles = Arrays.asList(uploadedDealsFileDto1, uploadedDealsFileDto2);

        when(mockUploadedDealsFileService.getUploadedFiles()).thenReturn(uploadedFiles);


    }


    @Test
    public void givenGetUploadedFileRequest_whenDisplayingFileListInIndexPage_thenReturnFileList() {
        Assert.assertTrue(mockUploadedDealsFileService.getUploadedFiles().size() > 0);
    }

}
