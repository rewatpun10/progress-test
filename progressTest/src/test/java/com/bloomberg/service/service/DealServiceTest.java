package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rpun on 1/27/2018.
 */


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@EnableJpaRepositories
public class DealServiceTest {


    @Mock
    DealService dealService;



    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        dealService = mock(DealService.class);

        final String fileName = "dealsample12.csv";
        Deals dealSample = new Deals();
        dealSample.setStartDate(new Date());
        dealSample.setEndDate(new Date());
        dealSample.setId(1L);
        dealSample.setFileName(fileName);
        when(dealService.getDealByFileName(fileName)).thenReturn(dealSample);

        final String fileNameNotSavedInDealsDB = "dealsample12.csv";
        when(dealService.saveDeal(fileNameNotSavedInDealsDB)).thenReturn(dealSample);

    }


    @Test
    public void givenValidCsvFile_whenUploading_thenDealShouldBeSaved() {
        String dealFileName = "dealsample12.csv";
        Deals deals = dealService.saveDeal(dealFileName);
        Assert.assertNotNull(deals);
        Assert.assertEquals("dealsample12.csv", deals.getFileName());

    }

    @Test
    public void givenCsvFile_whenUploading_thenDealsShouldNotBeSavedIfDealAreadyExist() {
        String fileName = "dealsample12.csv";
        Deals deal = dealService.getDealByFileName(fileName);
        Assert.assertNotNull(deal);


    }
}
