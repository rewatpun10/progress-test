package com.bloomberg.controller;

import com.bloomberg.service.service.UploadedDealsFileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by rpun on 1/22/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class MainPageControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UploadedDealsFileService uploadedDealsFileService;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new MainPageController(uploadedDealsFileService))
                .build();

    }

    @Test
    public void givenUrl_whenGetRequestOnMainController_thenRedirectToIndexPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.request(HttpMethod.GET, "/"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("index"))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }

}
