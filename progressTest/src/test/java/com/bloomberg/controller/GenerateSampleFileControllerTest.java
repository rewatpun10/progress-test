package com.bloomberg.controller;

import com.bloomberg.BloombergApplication;
import com.bloomberg.service.service.UploadedDealsFileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by rpun on 1/17/2018.
 */
@SpringBootTest(classes = { BloombergApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

public class GenerateSampleFileControllerTest {

    private MockMvc mockMvc;

    @Mock
    public UploadedDealsFileService uploadedDealsFileService;


    @Before
    public void init(){
        mockMvc = MockMvcBuilders
                .standaloneSetup(new GenerateSampleFileController(uploadedDealsFileService))
                .build();
    }

    @Test
    public void givenUrl_whenGeneratingFile_thenFileShouldBeGenerated() throws Exception {
     mockMvc.perform(MockMvcRequestBuilders.request(HttpMethod.POST,"/generateSampleFile"))
             .andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, HttpStatus.OK.value());
    }
}
