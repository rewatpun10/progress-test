package com.bloomberg.controller;

//import com.bloomberg.properties.AppProperties;

import com.bloomberg.service.service.FileService;
import com.bloomberg.service.service.UploadedDealsFileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by rpun on 1/20/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestPropertySource("/application.properties")
public class FileUploadControllerTest {

    private MockMvc mockMvc;

    @Mock
    private FileService fileService;

    @Value("${bloomberg.sampleFilePath}")
    private String filePath;

    @Mock
    private UploadedDealsFileService uploadedDealsFileService;


    @Before
    public void setUp() throws Exception {

        mockMvc = MockMvcBuilders
                .standaloneSetup(new FileUploadController(fileService, uploadedDealsFileService))
                .build();

    }

    @Test
    public void givenCsvFile_whenUploading_thenIfFileIsEmptyShouldNotBeSaved() throws Exception {
        InputStream inputStream = new FileInputStream(filePath + "deals20180119195817.csv");
        MockMultipartFile file = new MockMultipartFile("file", "", null, inputStream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadFile").file(file))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("index"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("fileUploadMessage"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void givenCsvFile_whenUploading_thenFileWithValidAndInvalidDealShouldBeSaved() throws Exception {
        InputStream inputStream = new FileInputStream(filePath + "deals20180119195817.csv");
        MockMultipartFile file = new MockMultipartFile("file", "fileName", null, inputStream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadFile").file(file))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("index"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
