package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import com.bloomberg.repo.DealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rpun on 1/21/2018.
 */
@Service
public class DealService {

    private final DealRepository dealRepository;

    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Autowired
    public DealService(DealRepository dealRepository) {
        this.dealRepository = dealRepository;
    }

    public Deals saveDeal(String fileName) {
        Deals dealFile = new Deals();
        dealFile.setFileName(fileName);
        dealFile.setStartDate(new Date());
        return dealRepository.save(dealFile);
    }


    public Deals getDealByFileName(String fileName) {
        return dealRepository.getDealByFileName(fileName);
    }

    public void saveDealwithCurrentEndDate(String fileName) {
        Deals deal = dealRepository.getDealByFileName(fileName);
        if (deal != null) {
            deal.setEndDate(new Date());
            dealRepository.save(deal);
        }
    }
}

