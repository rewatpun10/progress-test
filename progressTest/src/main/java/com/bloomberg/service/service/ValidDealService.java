package com.bloomberg.service.service;

import com.bloomberg.domain.ValidDeals;
import com.bloomberg.repo.ValidDealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rpun on 1/26/2018.
 */
@Service
public class ValidDealService {

    private ValidDealRepository validDealRepository;

    @Autowired
    public ValidDealService(ValidDealRepository validDealRepository) {
        this.validDealRepository = validDealRepository;
    }

    public List<ValidDeals> getValidDealByFileName(String fileName) {
        return validDealRepository.getValidDealsByFileName(fileName);
    }
}
