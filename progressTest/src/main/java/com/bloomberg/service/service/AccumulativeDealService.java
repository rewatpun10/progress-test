package com.bloomberg.service.service;

import com.bloomberg.domain.AccumulateDealsCount;
import com.bloomberg.domain.ValidDeals;
import com.bloomberg.repo.AccumulativeDealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by rpun on 1/21/2018.
 */
@Service
public class AccumulativeDealService {

    private final AccumulativeDealRepository accumulativeDealRepository;

    @Autowired
    public AccumulativeDealService(AccumulativeDealRepository accumulativeDealRepository) {
        this.accumulativeDealRepository = accumulativeDealRepository;
    }

    public Map<String, Long> getTotalValidAccumulativeDeals(List<ValidDeals> validDeals) {

        List<AccumulateDealsCount> getOldAccumulatedDealsList = accumulativeDealRepository.getAccumulateDealsCountsByFromCurrencyISOCode();

        Map<String, Long> countsOfOldAccumulativeDealsByCurrencyIsoCode = getOldAccumulatedDealsList.stream().collect(Collectors.toMap
                (AccumulateDealsCount::getCurrencyIsoCode, p -> (long) p.getDealsCount()));


        Map<String, Long> countsOfNewAccumulativeDealsByCurrencyIsoCode = validDeals.stream().collect(Collectors.groupingBy(ValidDeals::getFromCurrencyIsoCode, Collectors.counting()));

        return Stream.of(countsOfOldAccumulativeDealsByCurrencyIsoCode, countsOfNewAccumulativeDealsByCurrencyIsoCode)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                Long::sum
                        )
                );


    }

    public void saveNewAndUpdateExistingAccumulativeDeals(Map<String, Long> totalAccumulativeDeals) {
        for (Map.Entry<String, Long> entry : totalAccumulativeDeals.entrySet()) {
            AccumulateDealsCount accumulateDealsCount = getCountByCurrencyIsoCode(entry);
            if (accumulateDealsCount != null) {
                setCurrencyIsoCodeAndDealsCount(entry, accumulateDealsCount);
            } else {
                accumulateDealsCount = new AccumulateDealsCount();
                setCurrencyIsoCodeAndDealsCount(entry, accumulateDealsCount);

            }
            accumulativeDealRepository.save(accumulateDealsCount);
        }
    }

    private void setCurrencyIsoCodeAndDealsCount(Map.Entry<String, Long> entry, AccumulateDealsCount accumulateDealsCount) {
        accumulateDealsCount.setCurrencyIsoCode(entry.getKey());
        accumulateDealsCount.setDealsCount(entry.getValue().intValue());
    }

    private AccumulateDealsCount getCountByCurrencyIsoCode(Map.Entry<String, Long> entry) {
        return accumulativeDealRepository.getAccumulateDealsCountByCurencyIsoCode(entry.getKey());
    }
}
