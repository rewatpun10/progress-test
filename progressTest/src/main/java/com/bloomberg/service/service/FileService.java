package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import com.bloomberg.domain.InvalidDeals;
import com.bloomberg.domain.ValidDeals;
import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.repo.InvalidDealCustomRepository;
import com.bloomberg.repo.ValidDealCustomRepository;
import com.bloomberg.utils.CsvParserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by rpun on 1/19/2018.
 */
@Service
@Slf4j
@PropertySource("classpath:application.properties")
@Configuration
public class FileService {

    public static final String CSV = "csv";

    private final DealService dealService;

    private final DealProcessorService dealProcessorService;

    private final AccumulativeDealService accumulativeDealService;

    private InvalidDealCustomRepository invalidDealCustomRepository;

    private ValidDealCustomRepository validDealCustomRepository;

    private String uploadedFilePath;

    @Value("${bloomberg.UploadedFilePath}")
    public void setUploadedFilePath(String uploadedFilePath) {
        this.uploadedFilePath = uploadedFilePath;
    }

    @Autowired
    public FileService(DealService dealService, DealProcessorService dealProcessorService, AccumulativeDealService accumulativeDealService, InvalidDealCustomRepository invalidDealCustomRepository, ValidDealCustomRepository validDealCustomRepository) {
        this.dealService = dealService;
        this.dealProcessorService = dealProcessorService;
        this.accumulativeDealService = accumulativeDealService;
        this.validDealCustomRepository = validDealCustomRepository;
        this.invalidDealCustomRepository = invalidDealCustomRepository;
    }

    public String startUploadFile(MultipartFile file) throws FileNotFoundException {
        String message = "";
        try {

            String fileExtension = getUploadedFileExtension(file);
            //parse a CSV file and return deals
            List<BaseDealsDto> dealsDTO = loadDealFromCsvFile(file);

            if (isValidFileExtension(fileExtension)) {
                message = "Please upload file with .csv extension.";
                return message;
            }

            if (checkFileIsAlreadyUploaded(file)) {
                message = "File already exists";
                return message;
            }

            //save a uploaded csv file
            Deals deal = saveNewDeal(file);

            //get list of valid and invalid deal from the uploaded csv file
            List<ValidDeals> validDeals = getValidDeal(dealsDTO, deal);
            List<InvalidDeals> inValidDeals = getInvalidDeal(dealsDTO, deal);

            //accumulate the total number of valid deals with respective to their CurrencyISO code
            Map<String, Long> totalAccumulativeDeals = accumulativeDealService.getTotalValidAccumulativeDeals(validDeals);
            accumulativeDealService.saveNewAndUpdateExistingAccumulativeDeals(totalAccumulativeDeals);


            //group valid and invalid callables for execution
            groupValidAndInvalidCallableAndSave(validDeals, inValidDeals);

            //set deal end date and save
            dealService.saveDealwithCurrentEndDate(file.getOriginalFilename());
            message = "File uploaded successfully.";

        } catch (IOException e) {
            message = "File upload failed at the moment";
            log.error(e.getMessage());
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }


        return message;
    }

    private Deals saveNewDeal(MultipartFile file) {
        return dealService.saveDeal(file.getOriginalFilename());
    }

    private void groupValidAndInvalidCallableAndSave(List<ValidDeals> validDeals, List<InvalidDeals> inValidDeals) throws InterruptedException {
        List<Callable<List<ValidDeals>>> validCallables = groupValidCallables(validDeals);
        List<Callable<List<InvalidDeals>>> invalidCallables = groupInvalidCallables(inValidDeals);

        //execute list of valid callables and invalid callables to save
        executeCallablesToSave(validCallables, invalidCallables);
    }

    private List<InvalidDeals> getInvalidDeal(List<BaseDealsDto> dealsDTO, Deals deal) {
        return dealProcessorService.getInvalidDeals(dealsDTO, deal);
    }

    private List<ValidDeals> getValidDeal(List<BaseDealsDto> dealsDTO, Deals deal) {
        return dealProcessorService.getValidDeals(dealsDTO, deal);
    }

    private boolean checkFileIsAlreadyUploaded(MultipartFile file) {
        return dealService.getDealByFileName(file.getOriginalFilename()) != null;

    }

    private boolean isValidFileExtension(String fileExtension) {
        return !CSV.equalsIgnoreCase(fileExtension);

    }

    private List<BaseDealsDto> loadDealFromCsvFile(MultipartFile file) throws IOException {
        return CsvParserUtil.getDatasFromCsvFile(writeUpLoadedFileAndGetFilePath(file).toString());

    }

    private String getUploadedFileExtension(MultipartFile file) {
        return CsvParserUtil.getFileExtension(file);
    }

    private void executeCallablesToSave(List<Callable<List<ValidDeals>>> validCallables, List<Callable<List<InvalidDeals>>> invalidCallables) throws InterruptedException {

        ExecutorService validExecutorService = Executors.newFixedThreadPool(10);
        ExecutorService invalidExecutorService = Executors.newFixedThreadPool(10);

        for (Callable callable : validCallables) {
            validExecutorService.submit(callable);
        }

        for (Callable callable : invalidCallables) {
            invalidExecutorService.submit(callable);

        }

        validExecutorService.invokeAll(validCallables);

        invalidExecutorService.invokeAll(invalidCallables);

        validExecutorService.shutdown();
        invalidExecutorService.shutdown();
    }

    private List<Callable<List<InvalidDeals>>> groupInvalidCallables(List<InvalidDeals> inValidDeals) {
        List<Callable<List<InvalidDeals>>> invalidCallables = new ArrayList<>();
        int countOfDeals = 0;
        for (int i = 1; i <= inValidDeals.size(); i++) {
            if (i % 1000 == 0) {
                countOfDeals = 0;
                List<InvalidDeals> chunkOfInvalidDeals = new ArrayList<>();
                chunkOfInvalidDeals.addAll(inValidDeals.subList(i - 1000, i));
                invalidCallables.add(() -> {
                    invalidDealCustomRepository.saveDeals(chunkOfInvalidDeals);
                    return chunkOfInvalidDeals;
                });

            }
        }

        if (countOfDeals > 0) {
            List<InvalidDeals> chunkOfInvalidDeals = new ArrayList<>();
            chunkOfInvalidDeals.addAll(inValidDeals.subList(inValidDeals.size() - countOfDeals, inValidDeals.size()));
            invalidCallables.add(() -> {
                invalidDealCustomRepository.saveDeals(chunkOfInvalidDeals);
                return chunkOfInvalidDeals;
            });
        }

        return invalidCallables;
    }

    private List<Callable<List<ValidDeals>>> groupValidCallables(List<ValidDeals> validDeals) {
        List<Callable<List<ValidDeals>>> validCallables = new ArrayList<>();
        int countOfDeals = 0;
        for (int i = 1; i <= validDeals.size(); i++) {
            countOfDeals++;
            if ((i % 1000) == 0) {
                countOfDeals = 0;
                List<ValidDeals> chunkOfValidDeals = new ArrayList<>();
                chunkOfValidDeals.addAll(validDeals.subList(i - 1000, i));
                validCallables.add(() -> {
                    validDealCustomRepository.saveDeals(chunkOfValidDeals);
                    return chunkOfValidDeals;
                });
            }
        }

        if (countOfDeals > 0) {
            List<ValidDeals> chunkOfValidDeals = new ArrayList<>();
            chunkOfValidDeals.addAll(validDeals.subList(validDeals.size() - countOfDeals, validDeals.size()));
            validCallables.add(() -> {
                validDealCustomRepository.saveDeals(chunkOfValidDeals);
                return chunkOfValidDeals;
            });
        }

        return validCallables;
    }

    private Path writeUpLoadedFileAndGetFilePath(MultipartFile file) throws IOException {
        byte[] fileByteArray = file.getBytes();
        Path filePath = Paths.get(uploadedFilePath + file.getOriginalFilename());
        Files.write(filePath, fileByteArray);
        return filePath;
    }

}

