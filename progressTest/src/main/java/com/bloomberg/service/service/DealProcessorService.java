package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import com.bloomberg.domain.InvalidDeals;
import com.bloomberg.domain.ValidDeals;
import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.utils.DealsValidationUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rpun on 1/21/2018.
 */
@Service
public class DealProcessorService {
    public List<ValidDeals> getValidDeals(List<BaseDealsDto> dealsDTOs, Deals deal) {
        return dealsDTOs.stream().filter(dealDTO -> DealsValidationUtil.isDealValid(dealDTO))
                .map(dealDTO -> new ValidDeals(dealDTO, deal))
                .collect(Collectors.toList());
    }

    public List<InvalidDeals> getInvalidDeals(List<BaseDealsDto> dealsDtos, Deals deal) {
        return dealsDtos.stream().filter(dealDTO -> !DealsValidationUtil.isDealValid(dealDTO))
                .map(dealDTO -> new InvalidDeals(dealDTO, deal))
                .collect(Collectors.toList());
    }
}
