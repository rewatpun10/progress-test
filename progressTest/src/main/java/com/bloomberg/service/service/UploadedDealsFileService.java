package com.bloomberg.service.service;

import com.bloomberg.domain.Deals;
import com.bloomberg.dto.UploadedDealsFileDto;
import com.bloomberg.repo.DealRepository;
import com.bloomberg.repo.InvalidDealRepository;
import com.bloomberg.repo.ValidDealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rpun on 1/21/2018.
 */
@Service
public class UploadedDealsFileService {

    private final DealRepository dealRepository;

    private final InvalidDealRepository invalidDealRepository;

    private final ValidDealRepository validDealRepository;

    @Autowired
    public UploadedDealsFileService(DealRepository dealRepository, InvalidDealRepository invalidDealRepository, ValidDealRepository validDealRepository) {
        this.dealRepository = dealRepository;
        this.invalidDealRepository = invalidDealRepository;
        this.validDealRepository = validDealRepository;
    }

    public List<UploadedDealsFileDto> getUploadedFiles() {
        List<Deals> deals = dealRepository.findAll();
        List<UploadedDealsFileDto> totalUploadedDealFileDto = new ArrayList<>();
        for (Deals deal : deals) {
            UploadedDealsFileDto uploadedDealsFileDto = new UploadedDealsFileDto();
            Integer validUploadedDealsCount = validDealRepository.getValidDealsCountByFileName(deal.getFileName());
            Integer invalidUploadedDealsCount = invalidDealRepository.getInValidDealsCountByFileName(deal.getFileName());
            uploadedDealsFileDto.setFileName(deal.getFileName());
            uploadedDealsFileDto.setDealStartDate(deal.getStartDate());
            uploadedDealsFileDto.setDealEndDate(deal.getEndDate());
            uploadedDealsFileDto.setValidDealsCount(validUploadedDealsCount);
            uploadedDealsFileDto.setInvalidDealsCount(invalidUploadedDealsCount);
            totalUploadedDealFileDto.add(uploadedDealsFileDto);

        }
        return totalUploadedDealFileDto;

    }
}
