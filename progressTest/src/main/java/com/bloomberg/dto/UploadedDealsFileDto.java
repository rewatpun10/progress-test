package com.bloomberg.dto;

import java.util.Date;

/**
 * Created by rpun on 1/21/2018.
 */
public class UploadedDealsFileDto {

    private String fileName;

    private Integer validDealsCount;

    private Integer invalidDealsCount;

    private Date dealStartDate;

    private Date dealEndDate;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getValidDealsCount() {
        return validDealsCount;
    }

    public void setValidDealsCount(int validDealsCount) {
        this.validDealsCount = validDealsCount;
    }

    public int getInvalidDealsCount() {
        return invalidDealsCount;
    }

    public void setInvalidDealsCount(int invalidDealsCount) {
        this.invalidDealsCount = invalidDealsCount;
    }

    public Date getDealStartDate() {
        return dealStartDate;
    }

    public void setDealStartDate(Date dealStartDate) {
        this.dealStartDate = dealStartDate;
    }

    public Date getDealEndDate() {
        return dealEndDate;
    }

    public void setDealEndDate(Date dealEndDate) {
        this.dealEndDate = dealEndDate;
    }

}
