package com.bloomberg.controller;

import com.bloomberg.dto.UploadedDealsFileDto;
import com.bloomberg.service.service.UploadedDealsFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by rpun on 1/21/2018.
 */
@Controller
public class MainPageController {

    private final UploadedDealsFileService uploadedDealsFileService;

    @Autowired
    public MainPageController(UploadedDealsFileService uploadedDealsFileService) {
        this.uploadedDealsFileService = uploadedDealsFileService;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getUploadedDealsFile(ModelMap modelMap) {
        List<UploadedDealsFileDto> getUploadedDealsFile = uploadedDealsFileService.getUploadedFiles();
        modelMap.put("uploadedFiles", getUploadedDealsFile);
        return "index";
    }
}
