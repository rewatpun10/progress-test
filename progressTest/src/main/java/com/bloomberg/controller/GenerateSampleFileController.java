package com.bloomberg.controller;

import com.bloomberg.dto.UploadedDealsFileDto;
import com.bloomberg.service.service.UploadedDealsFileService;
import com.bloomberg.utils.FileGeneratorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by rpun on 1/15/2018.
 */
@Controller
@Slf4j
public class GenerateSampleFileController {


    private final UploadedDealsFileService uploadedDealsFileService;

    @Autowired
    public GenerateSampleFileController(UploadedDealsFileService uploadedDealsFileService) {
        this.uploadedDealsFileService = uploadedDealsFileService;
    }

    @RequestMapping(method = RequestMethod.POST , value = "/generateSampleFile")
    public String generateSampleFile(ModelMap modelMap){
        try {
            log.debug("Processing to generate a file");
            String message = FileGeneratorUtils.generateSampleFile();
            modelMap.put("result", message);
            List<UploadedDealsFileDto> uploadedDealsFile = uploadedDealsFileService.getUploadedFiles();
            modelMap.put("uploadedFiles", uploadedDealsFile);
        }catch (Exception e){
            modelMap.put("result", "File cannot be generated at the moment");
        }
        log.debug("Sent message {} ", modelMap.get("result"));
        return "index";
    }
}
