package com.bloomberg.controller;

import com.bloomberg.dto.UploadedDealsFileDto;
import com.bloomberg.service.service.FileService;
import com.bloomberg.service.service.UploadedDealsFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by rpun on 1/19/2018.
 */
@Controller
@Slf4j
public class FileUploadController {

    private final FileService fileService;

    private final UploadedDealsFileService uploadedDealsFileService;

    @Autowired
    public FileUploadController(FileService fileService, UploadedDealsFileService uploadedDealsFileService) {
        this.fileService = fileService;
        this.uploadedDealsFileService = uploadedDealsFileService;
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String uploadFile(@RequestParam("file") MultipartFile file, ModelMap modelMap) {

        if (file.getOriginalFilename().isEmpty()) {
            List<UploadedDealsFileDto> uploadedFiles = uploadedDealsFileService.getUploadedFiles();
            modelMap.put("uploadedFiles", uploadedFiles);
            modelMap.put("fileUploadMessage", "No file uploaded!!!");
            return "index";
        }

        try {
            String fileUploadMessage = fileService.startUploadFile(file);
            modelMap.put("fileUploadMessage", fileUploadMessage);
            List<UploadedDealsFileDto> uploadedFiles = uploadedDealsFileService.getUploadedFiles();
            modelMap.put("uploadedFiles", uploadedFiles);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return "index";
    }

}
