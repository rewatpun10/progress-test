package com.bloomberg.repo;

import com.bloomberg.domain.ValidDeals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * Created by rpun on 1/21/2018.
 */
public interface ValidDealRepository extends JpaRepository<ValidDeals, Long> {

    @Query("SELECT count(vd) FROM ValidDeals vd where vd.fileName = :fileName")
    public Integer getValidDealsCountByFileName(@Param("fileName") String fileName);

    List<ValidDeals> getValidDealsByFileName(String fileName);
}
