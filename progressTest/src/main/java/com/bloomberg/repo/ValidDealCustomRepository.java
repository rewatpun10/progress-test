package com.bloomberg.repo;

import com.bloomberg.domain.ValidDeals;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by rpun on 1/20/2018.
 */
@Component
@Slf4j
public class ValidDealCustomRepository {

    private ValidDealRepository validDealRepository;

    public ValidDealCustomRepository(ValidDealRepository validDealRepository) {
        this.validDealRepository = validDealRepository;
    }

    public void saveDeals(List<ValidDeals> validDeals) {
        validDealRepository.save(validDeals);
    }
}