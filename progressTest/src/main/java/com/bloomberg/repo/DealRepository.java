package com.bloomberg.repo;

import com.bloomberg.domain.Deals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Created by rpun on 1/20/2018.
 */
@Repository
public interface DealRepository extends JpaRepository<Deals, Long> {

    @Query("select d from Deals d where d.fileName = :fileName")
    Deals getDealByFileName(@Param("fileName") String fileName);


}
