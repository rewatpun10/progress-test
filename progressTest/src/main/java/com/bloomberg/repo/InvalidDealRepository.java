package com.bloomberg.repo;

import com.bloomberg.domain.InvalidDeals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by rpun on 1/21/2018.
 */
public interface InvalidDealRepository extends JpaRepository<InvalidDeals, Long> {

    @Query("SELECT count(ind) FROM InvalidDeals ind where ind.fileName = :fileName")
    public Integer getInValidDealsCountByFileName(@Param("fileName") String fileName);
}
