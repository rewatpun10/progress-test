package com.bloomberg.repo;

import com.bloomberg.domain.InvalidDeals;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by rpun on 1/20/2018.
 */
@Component
@Slf4j
public class InvalidDealCustomRepository {

    private final InvalidDealRepository invalidDealRepository;



    public InvalidDealCustomRepository(InvalidDealRepository invalidDealRepository) {
        this.invalidDealRepository = invalidDealRepository;
    }

    public void saveDeals(List<InvalidDeals> invalidDeals) {
        invalidDealRepository.save(invalidDeals);


    }
}
