package com.bloomberg.repo;

import com.bloomberg.domain.AccumulateDealsCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rpun on 1/21/2018.
 */
@Repository
public interface AccumulativeDealRepository extends JpaRepository<AccumulateDealsCount, Long> {

    @Query("SELECT ac FROM AccumulateDealsCount ac GROUP BY ac.currencyIsoCode")
    List<AccumulateDealsCount> getAccumulateDealsCountsByFromCurrencyISOCode();

    @Query("SELECT ac from AccumulateDealsCount ac where ac.currencyIsoCode = :currencyIsoCode")
    AccumulateDealsCount getAccumulateDealsCountByCurencyIsoCode(@Param("currencyIsoCode") String currencyIsoCode);
}
