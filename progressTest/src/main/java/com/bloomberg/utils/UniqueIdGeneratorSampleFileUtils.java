package com.bloomberg.utils;

import java.util.UUID;

/**
 * Created by rpun on 1/16/2018.
 */
public class UniqueIdGeneratorSampleFileUtils {

    public static final String getRandomGeneratedUniqueIdForSampleFile(){
        return UUID.randomUUID().toString();
    }
}
