package com.bloomberg.utils;

import java.util.Random;

/**
 * Created by rpun on 1/16/2018.
 */
public class AmountGeneratorSampleFileUtils {
   private static final double MIN =10;
   private static final double MAX = 1000;

    public static double getValidAmount(){
        return Math.round(MIN + (MAX - MIN) * new Random().nextDouble() * 100d) / 100d;
    }


    public static double getInValidAmount(){
        return Math.round(MIN + (MIN - MAX) * new Random().nextDouble() * 100d) / 100d;
    }
}
