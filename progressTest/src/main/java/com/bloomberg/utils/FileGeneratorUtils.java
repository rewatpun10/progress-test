package com.bloomberg.utils;

import com.bloomberg.dto.BaseDealsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import com.bloomberg.properties.AppProperties;

/**
 * Created by rpun on 1/16/2018.
 */

@Component
@Slf4j
public class FileGeneratorUtils {

    private static String filePath;

    static SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    public static String generateSampleFile() {

        String fileSample = filePath + File.separator + "deals" + TIMESTAMP_FORMAT.format(new Date()) + ".csv";

        List<BaseDealsDto> deals = new ArrayList<>();

        for (int a = 1; a <= 100000; a++) {
            if (a % 2 == 0) {
                generateValidData(deals);
            } else {
                generateInvalidData(deals);

            }
        }

        writeToCSVFile(fileSample, deals);

        log.info("Sample file generated successfully at location " + fileSample);

        return "Sample file generated successfully at location " + fileSample;

    }

    private static void writeToCSVFile(String fileSample, List<BaseDealsDto> deals) {
        FileWriterUtils fileWriterUtils = new FileWriterUtils();
        fileWriterUtils.writeDataToCSV(fileSample, deals);
    }

    private static void generateInvalidData(List<BaseDealsDto> deals) {
        BaseDealsDto invalidDeal = new BaseDealsDto();
        invalidDeal.setUniqueId(UniqueIdGeneratorSampleFileUtils.getRandomGeneratedUniqueIdForSampleFile());
        invalidDeal.setFromCurrencyIsoCode(CurrencyCodeGeneratorSampleFileUtils.getRandomInvalidCurrencyCode());
        invalidDeal.setToCurrencyIsoCode(CurrencyCodeGeneratorSampleFileUtils.getRandomValidCurrencyCode());
        invalidDeal.setAmount(AmountGeneratorSampleFileUtils.getInValidAmount());
        invalidDeal.setDealStartDate(getTimeStamp());
        deals.add(invalidDeal);
    }

    private static void generateValidData(List<BaseDealsDto> deals) {
        BaseDealsDto validDeal = new BaseDealsDto();
        validDeal.setUniqueId(UniqueIdGeneratorSampleFileUtils.getRandomGeneratedUniqueIdForSampleFile());
        validDeal.setFromCurrencyIsoCode(CurrencyCodeGeneratorSampleFileUtils.getRandomValidCurrencyCode());
        validDeal.setToCurrencyIsoCode(CurrencyCodeGeneratorSampleFileUtils.getRandomValidCurrencyCode());
        validDeal.setAmount(AmountGeneratorSampleFileUtils.getValidAmount());
        validDeal.setDealStartDate(getTimeStamp());
        deals.add(validDeal);
    }

    public static String getTimeStamp() {
        return DATE_FORMAT.format(new Date());
    }

    public String getFilePath() {
        return filePath;
    }

    @Value("${bloomberg.sampleFilePath}")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}