package com.bloomberg.utils;

import java.util.Random;

/**
 * Created by rpun on 1/16/2018.
 */
public class CurrencyCodeGeneratorSampleFileUtils {

    private static final String[] VALID_COUNTRY_CODE = new String[]{"NPR", "AUS", "GBP" ,"CAD", "USA" , "AED"};

    private static final String[] INVALID_COUNTRY_CODE = new String[]{"ZZZ", "CCC", "QQQ" ,"TTT", "PLZ" , "KQQ"};

    public static String getRandomValidCurrencyCode(){
        return VALID_COUNTRY_CODE[new Random().nextInt(VALID_COUNTRY_CODE.length)];
    }

    public static String getRandomInvalidCurrencyCode(){
        return INVALID_COUNTRY_CODE[new Random().nextInt(INVALID_COUNTRY_CODE.length)];
    }
}
