package com.bloomberg.utils;

import com.bloomberg.dto.BaseDealsDto;

import java.util.Arrays;

/**
 * Created by rpun on 1/21/2018.
 */
public class DealsValidationUtil {

    private static final String[] VALID_COUNTRY_CODE = new String[]{"NPR", "AUS", "GBP", "CAD", "USA", "AED"};


    public static boolean isValidDealAmount(Double dealAmount) {
        return dealAmount > 0;
    }


    public static boolean isValidCountryCode(String countryCode) {
        boolean result = false;
        if (Arrays.stream(VALID_COUNTRY_CODE).anyMatch(validCountryCode -> countryCode.contains(validCountryCode))) {
            result = true;
        }
        return result;
    }

    public static boolean isDealValid(BaseDealsDto baseDealsDto) {
        return (isValidCountryCode(baseDealsDto.getFromCurrencyIsoCode())
                && isValidCountryCode(baseDealsDto.getToCurrencyIsoCode())
                && isValidDealAmount(baseDealsDto.getAmount()));
    }

}
