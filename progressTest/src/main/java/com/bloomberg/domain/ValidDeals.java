package com.bloomberg.domain;

import com.bloomberg.dto.BaseDealsDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by rpun on 1/15/2018.
 */

@Entity
@Table(name = "valid_Deals")
@Getter
@Setter
public class ValidDeals extends AbstractBaseDeals {

    String uniqueId = super.getUniqueId();
    String fromCurrencyIsoCode = super.getFromCurrencyIsoCode();
    String toCurrencyIsoCode = super.getFromCurrencyIsoCode();
    Double amount = super.getAmount();
    String fileName;

    public ValidDeals() {

    }

    public ValidDeals(BaseDealsDto baseDealsDto, Deals deal) {
        uniqueId = baseDealsDto.getUniqueId();
        fromCurrencyIsoCode = baseDealsDto.getFromCurrencyIsoCode();
        toCurrencyIsoCode = baseDealsDto.getToCurrencyIsoCode();
        amount = baseDealsDto.getAmount();
        this.fileName = deal.getFileName();
        this.deals = deal;

    }

    @JoinColumn(name = "deals")
    @ManyToOne
    private Deals deals;


}
