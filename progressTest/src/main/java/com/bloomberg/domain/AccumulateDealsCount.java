package com.bloomberg.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rpun on 1/15/2018.
 */
@Entity
@Table(name = "accumulate_deals_count")
@Getter
@Setter
public class AccumulateDealsCount extends AbstractBaseEntity{

    @Column(name = "currency_iso_code")
    private String currencyIsoCode;

    @Column(name = "deals_count")
    private int dealsCount;

}
